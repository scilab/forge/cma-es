// Copyright (C) 2008 - INRIA
// Copyright (C) 2009-2011 - DIGITEO

// This file is released under the 3-clause BSD license. See COPYING-BSD.

mode(-1);
lines(0);


TOOLBOX_NAME  = "cma_es";
TOOLBOX_TITLE = "CMA-ES Optimization Toolbox";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab's version
// =============================================================================

try
  v = getversion("scilab");
catch
  error(gettext("Scilab 5.5.1 or more is required."));
end


// Check modules_manager module availability
// =============================================================================

// Action
// =============================================================================

tbx_builder_macros(toolbox_dir); // not tested
tbx_builder_help(toolbox_dir);
tbx_build_loader(toolbox_dir); // generate loader.sce
tbx_build_cleaner(toolbox_dir); // generate cleaner.sce script

