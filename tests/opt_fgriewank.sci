function f=opt_fgriewank(x)
// in -600..600
  f = 1 + sum(x.^2)/4000 + prod(cos(x./sqrt(1:size(x,2))'));
endfunction
