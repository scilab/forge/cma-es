function f=opt_fspherecorr(x)
   f=1e4*(x(1) - x(2))^2 + 1e0*sum(x(3:$).^2);
endfunction
