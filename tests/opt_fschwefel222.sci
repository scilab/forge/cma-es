function f=opt_fschwefel222(x)
  f = sum(abs(x)) + prod(abs(x));
endfunction
