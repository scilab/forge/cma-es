function f=opt_fdiffpow(x)
// parallelized
N = size(x,1);
popsize = size(x,2);
if (N < 2) then error('dimension must be greater than one'); end
f=sum(abs(x).^((2+10*(0:N-1)'/(N-1)) * ones(1,popsize)), 1);
endfunction
