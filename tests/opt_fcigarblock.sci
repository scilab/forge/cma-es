function f=opt_fcigarblock(x)
  N = size(x,1);
  f = opt_fcigar(opt_blockcoordinatesystem(N)*x);
endfunction
