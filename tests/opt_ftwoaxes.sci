function f=opt_ftwoaxes(x)
f = sum(x(1:$/2).^2) + 1e6*sum(x(1+$/2:$).^2);
endfunction
