function f=opt_fsharpR(x)
f = -x(1) + 100*norm(x(2:$));
endfunction
