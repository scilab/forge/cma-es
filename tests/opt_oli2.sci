// a parabolic ridge in each diagonal (2^N ridges)
// proposed by Olivier, realized by Niko
function f=opt_oli2(x)
  N = size(x,1);
  r = sqrt(sum(x.^2,1));
  v = sign(x) / sqrt(N); // closest diagonal
  vdifference = x - (v'*x) * v; // difference vector to diagonal
  dist = sqrt(sum(vdifference.^2,1)); // distance to diagonal
  f = 1*r^2 + 1e4*(dist)^2 / 1;
  // f = sum(x.^40,1);
  // f = sum(x.^2,1) + (N - (sum(abs(x))/sqrt(sum(x.^2)))^2);
endfunction
