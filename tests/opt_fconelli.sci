function [f, g] = opt_fconelli(x)
  N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
  f=1e6.^((0:N-1)/(N-1)) * x.^2;

  con.M = min(3,N); // number of constraints
  inc = floor((N-1)/(con.M-1));

  con.idx = 1:inc:1+(con.M-1)*inc;
  con.arweight = 1; // edit this
  con.arvalue = 0.1;
  g = con.arweight .* (x(con.idx) + con.arvalue);

endfunction
