function f=opt_fcigtab(x)
f = x(1)^2 + 1e8*x($)^2 + 1e4*sum(x(2:($-1)).^2);
endfunction
