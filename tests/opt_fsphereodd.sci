function f=opt_fsphereodd(x)
  f = sum(x(1:2:$,:).^2);
endfunction
