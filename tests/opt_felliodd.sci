function f=opt_felliodd(x) // only odd variables are used
N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
f=1e6.^((0:2:N-1)/(N-1)) * x(1:2:$,:).^2;
endfunction
