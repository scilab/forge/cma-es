function f=opt_fcigar(x)
f = x(1)^2 + 1e6*sum(x(2:$).^2);
endfunction
