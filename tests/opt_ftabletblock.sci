function f=opt_ftabletblock(x)
  N = size(x,1);
  f = opt_ftablet(opt_blockcoordinatesystem(N)*x);
endfunction
