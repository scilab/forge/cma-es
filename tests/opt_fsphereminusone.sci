function f=opt_fsphereminusone(x)
  f=sum((x-1).^2);
endfunction
