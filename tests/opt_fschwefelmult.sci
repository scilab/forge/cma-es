function f=opt_fschwefelmult(x)
  f = sum(x .* sin(sqrt(abs(x))));
  f = f + 1e-3*(sum(x(x<-500).^2) + sum(x(x>500).^2));
endfunction
