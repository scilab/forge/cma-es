function f=opt_fvardim(x)
// Buckley 1989 page 98 according to Powell 2004
// Powells improved DFO is about 2 times faster than CMA-ES
// should work parallelized
  N = size(x,1);
  tmp = (1:N) * (x-1); // is a vector if x is a population
  f = sum((x-1).^2, 1) + tmp.^2 + tmp.^4;
endfunction
