function ar = opt_genblockcoordinatesystem(N)
  for N = N
    ar = eye(N,N);
    for i = 1:2:N-1
      ar(i:i+1,i:i+1) = 1./sqrt(2);
      ar(i, i) = -ar(i, i);
    end
  end
endfunction
