// sphere with flat epsilon ball in the middle
function f=opt_fsphereconst(x)
   f=sum(x.^2);
   if f < 0.1
      f = 0.01;
   end
endfunction
