function f=opt_fsqrtnoise(x)
  f=sum(x.^2)^0.25 + 0.01 * rand(1,1,'normal');
endfunction
