// lesser known rosenbrock, also less difficult
function f=opt_flkrosen(x)
if (size(x,1) < 2) then error('dimension must be greater than one'); end
f = 1e2*sum((x(1:2:$).^2 - x(2:2:$)).^2) + sum((x(1:$-1)-1).^2);
// f = f + 0.01*rand(1,1,'normal'); // /rand(1,1,'normal');
endfunction
