function f=opt_fspherenoise(x)
  N = size(x,1);
  f=sum(x.^2) * (1 + (1/N/2) * rand(1,1,'normal')/rand(1,1,'normal') + 0/N*rand(1,1,'normal')) + 0. * (rand(1,1,'normal'));
endfunction
