function ar = opt_gencoordinatesystem(N)
  for N = N
    ar = rand(N,N,'normal');
    for i = 1:N
      for j = 1:i-1
	ar(:,i) = ar(:,i) - ar(:,i)'*ar(:,j) * ar(:,j);
      end
      ar(:,i) = ar(:,i) / norm(ar(:,i));
    end
  end
endfunction
