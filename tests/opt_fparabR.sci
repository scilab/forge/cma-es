function f=opt_fparabR(x)
f = -x(1) + 100*sum(x(2:$).^2)^(1.5/2);
endfunction
