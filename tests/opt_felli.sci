function f=opt_felli(x)
N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
f=1e6.^((0:N-1)/(N-1)) * x.^2;
endfunction
