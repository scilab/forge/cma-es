function f=opt_frastriginskew10(x)
N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
pop = size(x,2);
if pop > 1 then error('parallelized frastriginskew10 is not yet tested'); end
scale=10.^((0:N-1)'/(N-1));
// parallelize
scale = scale * ones(1,pop);
scale(x>0) = 1;
f = 10*size(x,1) + sum((scale.*x).^2 - 10*cos(2*%pi*(scale.*x)));
endfunction
