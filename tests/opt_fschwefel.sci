function f=opt_fschwefel(x)
f = 0;
for i = 1:size(x,1),
  f = f+sum(x(1:i))^2;
end
endfunction
