function f=opt_fdiffpow4(x)
N = size(x,1);
if (N < 2) then error('dimension must be greater than one'); end
f=sum(abs(x).^(2+4*(0:N-1)'/(N-1)));
endfunction
