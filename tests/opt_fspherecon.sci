function f=opt_fspherecon(x)
   // parallelized
   N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
   f=1e6.^((0:N-1)/(N-1)) * x.^2;
   // f=sum(x(1:$,:).^2, 1);
   // f = [f ; x(1,:)+1];
   f = [f ; (x([2:2:$],:) - 20+1e-5)];
//   f = [f ; ([-x(1,:) x([3:2:$],:)] + 20+1e-5)];
endfunction
