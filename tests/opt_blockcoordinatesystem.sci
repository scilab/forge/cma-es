function M = opt_blockcoordinatesystem(N)
  global BLOCKCOORSYS_G
  if type(BLOCKCOORSYS_G) ~= 15
    BLOCKCOORSYS_G = list();
  end
  if ~or(N==definedfields(BLOCKCOORSYS_G))
    BLOCKCOORSYS_G(N) = opt_genblockcoordinatesystem(N);
  end
  M = BLOCKCOORSYS_G(N);
endfunction
