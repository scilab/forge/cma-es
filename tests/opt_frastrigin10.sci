function f=opt_frastrigin10(x)
N = size(x,1); if (N < 2) then error('dimension must be greater than one'); end
scale=1.^((0:N-1)'/(N-1));
f = 10*size(x,1) + sum((scale.*x).^2 - 10*cos(2*%pi*(scale.*x)));
endfunction
