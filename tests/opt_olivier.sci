// infinite condition number function
function f=opt_olivier(x)
  N = size(x,1);
  r = sqrt(sum(x.^2,1));
  //  ang = 90*(1 - abs(x(1))/sqrt(sum(x.^2,1)));
  // cos angle(x,e_1) = <x,e_1> / ||x|| / ||e_1||
  ang = acos(abs(x(1)/r)); // x(1) == <x,e_1>
  f = sum(x.^2)^0.5 + ang^2;
endfunction
