function M = opt_coordinatesystem(N)
  global ORTHOCOORSYS_G
  if type(ORTHOCOORSYS_G) ~= 15
    ORTHOCOORSYS_G = list();
  end
  if ~or(N==definedfields(ORTHOCOORSYS_G))
    ORTHOCOORSYS_G(N) = opt_gencoordinatesystem(N);
  end
  M = ORTHOCOORSYS_G(N);
endfunction
