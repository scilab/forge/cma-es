function f=opt_ftabletrotselect(x, c) // c=countiter=generation
// select random subspace generational-wise
// works surprisingly well
   N=size(x,1);
   x=opt_coordinatesystem(N)*x;
   s = rand('seed');
   rand('seed', c);
   idx = rand(x)<0.7;
   rand('seed', s);
   f = 1e6*idx(1,:).*x(1,:)^2 + sum(x(idx).^2);
endfunction
