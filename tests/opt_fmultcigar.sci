function f=opt_fmultcigar(x)
N = size(x,1);
M = min(3,N);
f = sum(x(1:M).^2) + 1e6*sum(x(M+1:$).^2);
endfunction
