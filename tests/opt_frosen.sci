function f=opt_frosen(x) // Rosenbrock
if (size(x,1) < 2) then error('dimension must be greater than one'); end
f = 1e2*sum((x(1:$-1).^2 - x(2:$)).^2) + sum((x(1:$-1)-1).^2);
// f = f + 0.01*rand(1,1,'normal'); // /rand(1,1,'normal');
if rand(1) < -0.001
  f = %nan;
end
endfunction
