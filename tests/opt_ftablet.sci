function f=opt_ftablet(x)
f = 1e6*x(1)^2 + sum(x(2:$).^2);
endfunction
