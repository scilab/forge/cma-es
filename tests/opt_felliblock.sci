function f=opt_felliblock(x)
  N = size(x,1);
  f = opt_felli(opt_blockcoordinatesystem(N)*x);
endfunction
