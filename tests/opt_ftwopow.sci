function f=opt_ftwopow(x)
f = sum(abs(x(1:$/2)).^2) + 1e4 * sum(abs(x(1+$/2:$)).^4);
endfunction
