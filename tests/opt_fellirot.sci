function f=opt_fellirot(x)
  N = size(x,1);

  f = opt_felli(opt_coordinatesystem(N)*x);
endfunction
