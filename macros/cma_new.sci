////////////////////////////////////////////////////////////
// ------------------- CONSTRUCTOR -------------------------
////////////////////////////////////////////////////////////
function [es, p] = cma_new(inparam)

  // string 'cma' is the object type, mlist because of ??
  es = tlist(['cma', 'stop', 'param', 'out', 'inparam', 'defparam', ...
          'N', 'sp', 'countiter', 'counteval', ...
          'sigma', 'xmean', 'pc', 'ps', 'B', 'D', 'BD', 'C', 'fitnesshist', 'const', ...
          'verb', 'files', 'logfunvals']);
        // list must contain all variables

  es.defparam = cma_getdefaults();
  p = [];

  // return default parameters
  if ~isdef('inparam','local')
    es = es.defparam;
    printf('cma_new has two mandatory fields in its input parameter struct: ' ...
        +'\n   x0 (or typical_x) and sigma0. ' ...
        +'\nA complete parameter struct has been returned.\n')
    return;
  end
  if (typeof(inparam) == 'string' & inparam == 'getdefaults') ...
      | isempty(inparam)
    es = es.defparam;
    return;
  end

  es.inparam = inparam;
  p = check_and_merge_param(inparam, es.defparam);

  // keep empty readfromfile
  if isfield(inparam, 'verb') & isfield(inparam.verb, 'readfromfile')
    p.verb.readfromfile = inparam.verb.readfromfile;
  end

  es.out = [];
  es.out.seed = rand('seed'); // needs to be done before any input parameter is evaluated
  es.out.version = 0.998;


// -------------------- Handle Input Arguments -------------------------------

  // mandatory arguments
  p.x0 = evstr(p('x0'));
    x0 = p.x0; // for historical reasons, might be removed in near future
  p.typical_x = evstr(p('typical_x'));
    typical_x = p.typical_x; // for historical reasons, might be removed in near future
  if (isempty(x0) & isempty(typical_x))
    error('CMA-ES: x0 or typical_x must be defined');
  end

  // set dimension
  p.opt.scaling_of_variables = evstr(p.opt.scaling_of_variables);
  scaling_of_variables = cma_check_initial_sizes(typical_x, x0, p.opt.scaling_of_variables);
  es.N = sum(scaling_of_variables > 0);
  N = es.N; // for evstr()

  // set sigma
  p.sigma0 = evstr(p('sigma0')); // todo: remove this?
  if isempty(p.sigma0)
    error('sigma0 (initial one-sigma) must be defined');
  end
  if ~and(size(p.sigma0) == [1 1])
    error('input parameter sigma must be a scalar, use scaling_of_variables otherwise');
  end
  es.sigma = p.sigma0;
  sigma0 = p.sigma0; // for evstr()
  sigma = p.sigma0; // for evstr()

  //
  // TODO: check: sigma tolx stopping criteria are consistent with settings of sigma
  //       and of the gp-transformation?
  es.out.genopheno = list(); // otherwise the next assignment produces an error
  es.out.genopheno = cma_tlistgenopheno(typical_x, scaling_of_variables, x0);

  // ------ we need to know N and sigma from here on (not xmean)

  // --- optional arguments

  p.opt.lambda = evstr(p.opt.lambda);
  es.sp.lambda = p.opt.lambda;
  lambda = p.opt.lambda; // for evstr()
  if (lambda < 3) // even for 2-D it is unreliable
    error('param.opt.lambda must be larger than 2');
  end
  if (lambda < 4 & N > 4)
    warning('param.opt.lambda set too small (unreliable setting)');
  end

  p.opt.mu = evstr(p.opt.mu);
  es.sp.mu = p.opt.mu; // integer is not enforced, 1:mu is used, better should round up for trunc(mu)>0.5
  if ceil(es.sp.mu) > lambda
    error('param.opt.mu must be smaller than param.opt.lambda');
  end

  // verbosity
  logmodulo = p.verb.logmodulo; // for evstr()
  if p.verb.logmodulo == 0
    p.verb.plotmodulo = 0;
  end
  if ~isempty(p.verb.logfunvals) // array of decreasing function values when to record data
    p.verb.logfunvals = gsort(p.verb.logfunvals);
  end
  es.logfunvals = p.verb.logfunvals; // dynamic list, written elements are removed
  p.verb.plotmodulo = evstr(p.verb.plotmodulo);
  if 11 < 3 // | p.verb.silent
    p.verb.logmodulo = 0;
    p.verb.displaymodulo = 0;
    p.verb.plotmodulo = 0;
    if p.verb.silent > 0
      es.logfunvals = [];
    end
  end

  // termination
  p.stop.maxfunevals = evstr(p.stop.maxfunevals);
  p.stop.maxiter = evstr(p.stop.maxiter);
  p.stop.tolx = evstr(p.stop.tolx);
  p.stop.tolupx = evstr(p.stop.tolupx);
  p.stop.tolfun = evstr(p.stop.tolfun);
  p.stop.tolfunhist = evstr(p.stop.tolfunhist);

  ccovfac = 1;
  p.opt.separable = evstr(p.opt.separable); // iteration(N, lambda)
  es.sp.separable = p.opt.separable; // iteration(N, lambda)
  if es.sp.separable ~= 0
    ccovfac = (N+1.5)/3;
    if es.sp.separable == 1
      es.sp.separable = %inf;
    end
  end

  // -------------------- Initialization --------------------------------
  es.counteval = 0;
  es.out.dimension = N;
  // es.out.fitnessfunction = fitfun; // makes the struct non-investigable
  // es.out.stopflagss = list(); // for restarts

  // set xmean and sigma
  // TODO: check this
  // es.sigma = in.sigma; (obsolete? in case of restart?)
  clear sigma; // to be on the save side
  clear lambda; // TODO: clear all input parameters needed for getparam up to here for savity
  es.xmean = cma_phenogenotransform(es.out.genopheno, cma_xstart(typical_x, x0, scaling_of_variables));


  // --- Strategy parameter setting: Selection
  es.sp.weights = log(es.sp.mu+0.5)-log(1:es.sp.mu)'; // muXone array for weighted recombination
  // es.sp.lambda=12; es.sp.mu=3; weights = ones(es.sp.mu,1); disp('equal w''s'); // uncomment for (3_I,12)-ES
  es.sp.weights = es.sp.weights/sum(es.sp.weights);           // normalize recombination weights array
  es.sp.mueff = sum(es.sp.weights)^2/sum(es.sp.weights.^2); // variance-effective size of mu

  // Strategy parameter setting: Adaptation
  es.sp.cc = (4 + es.sp.mueff/N) / (N+4 + 2*es.sp.mueff/N);  // time const. for cumulation for covariance matrix
  if es.sp.separable > 2*sqrt(N)  // noticable difference only for N>>100
    es.sp.cc = (1+1/N + es.sp.mueff/N) / (N^0.5 + 1/N + 2.*es.sp.mueff/N)
//    es.sp.cc = (1+1/N + es.sp.mueff/N) / (N^0.5 + 1./N + 2.*es.sp.mueff/N)
  end
  es.sp.cs = (es.sp.mueff+2)/(N+es.sp.mueff+3);  // time const. for cumulation for sigma control
//  es.sp.cs = sqrt(es.sp.mueff)/(sqrt(N-1)+sqrt(es.sp.mueff)); disp('cs=' + string(es.sp.cs));
//  es.sp.cs    = (es.sp.mueff+2)/(sqrt(N)+es.sp.mueff+3); disp('cs=' + string(es.sp.cs));

  es.sp.mucov = es.sp.mueff;            // mu used for C update, mucov=1 ==> rank-1 only
//  es.sp.mucov = 1; disp('mucov=' + string(es.sp.mucov));
  es.sp.ccov_def = (1/es.sp.mucov) * 2/(N+1.4)^2 ...     // learning rate for covariance matrix
          + (1-1/es.sp.mucov) * ((2*es.sp.mucov-1)/((N+2)^2+2*es.sp.mucov));
  es.sp.ccov = ccovfac * es.sp.ccov_def;
  if es.sp.ccov > 1
    es.sp.ccov = 1;
    if ccovfac == 1
      warning('ccov set to one, due to a BUG? ');
    end
  end
  es.sp.damps = 1 + es.sp.cs ...                      // damping for sigma, usually close to 1
          + 2*max(0, sqrt((es.sp.mueff-1)/(N+1))-1);


// qqq hack different constants here
//  es.sp.cc = 1; disp('cc=' + string(es.sp.cc));
//  es.sp.damps = 0.5*es.sp.damps; 1e97; disp('damps=' + string(es.sp.damps)) // "turn off" sigma adaptation
 //es.sp.ccov = 0.0*es.sp.ccov; disp('es.sp.ccov=' + string(es.sp.ccov)); // turn off covariance matrix adaptation

  // Initialize dynamic (internal) strategy parameters and constants
  es.pc   = zeros(N,1);                   // evolution paths for C
  es.ps = zeros(N,1);                     //   and sigma
  es.B    = eye(N,N);                     // B defines the coordinate system
  es.D    = eye(N,N);                     // diagonal matrix D defines the scaling
  es.BD   = eye(N,N); // == es.B*es.D;       selling memory for speed
  es.C    = eye(N,N); // == es.BD*(es.BD)'   covariance matrix
  es.const.chiN = N^0.5*(1-1/(4*N)+1/(21*N^2)); // expectation of ||N(0,I)|| == norm(randn(N,1))
                                       // exact: sqrt(2) * gamma((N+1)/2) / gamma(N/2)
  es.fitnesshist = %nan*ones(10+ceil(30*N/es.sp.lambda),1); // history of fitness values

  // initialize constraints
//  if ~isdef('constraints_weights', 'local')
//    constraints_weights = [];
//  end
//  constraints_obj = constraints_init(N, es.sp.lambda, constraints_weights, ceil(2+N/1));

  // initial printed message
  if (p.verb.displaymodulo)
    printf('(%d/%d_W,%d)-CMA-ES ', es.sp.mu, es.sp.mu, es.sp.lambda);
    printf('(W=[');
    for i=1:min(3,length(es.sp.weights))
      printf('%2.0f,', 100*es.sp.weights(i));
    end
    printf('...]%%, mueff=%3.1f) in %d', es.sp.mueff, N);
    if N < length(es.out.genopheno.scaling)
      printf(' of %d', length(es.out.genopheno.scaling));
    end
    printf('-D\n');
    if es.sp.separable ~= 0
      printf('Covariance matrix is diagonal');
      if es.sp.separable > 1 & es.sp.separable < %inf
        printf(' for %d iterations (%d evaluations)', ...
            es.sp.separable, es.sp.separable*es.sp.lambda);
      end
      printf('!\n');
    end

    if p.verb.plotmodulo == 0 & p.verb.logmodulo
      printf('    for plotting call CMA_PLOT using Ctrl-C or another Scilab shell\n');
    end
    es.verb.dispannotationline = ...
        'Iter, Evals: Function Value   (worst) |Axis Ratio |idx:Min SD, idx:Max SD\n';
  end


  // Write headers to output data files
  if p.verb.logmodulo  | es.logfunvals
    es.files.additions = ...
        ['axlen', 'fit', 'stddev', 'xmean', 'xrecentbest'];
    //          ['axlen', 'disp', 'fit', 'xrecentbest', 'stddev', 'xmean'];
    if p.verb.append
      es.counteval = p.verb.append;
    else
      for name = es.files.additions
        [fid, err] = mopen(p.verb.filenameprefix+name+'.xml', 'w');
        if err ~= 0
          warning('could not open '+p.verb.filenameprefix+name+'.xml' + ' ');
          es.files.additions(find(es.files.additions == name)) = [];
        else
          mfprintf(fid, '%s\n', ...
              '<CMAES-OUTPUT version=""' + string(es.out.version) + '"">');
          mfprintf(fid, '  <NAME>'+name+'</NAME>\n');
          mfprintf(fid, '  <DATE>'+date()+'</DATE>\n');
          mfprintf(fid, '  <PARAMETERS>\n');
          mfprintf(fid, '    dimension=' + string(N) + '\n');
          mfprintf(fid, '  </PARAMETERS>\n');
          // different cases for DATA columns annotations here
          mfprintf(fid, '  <DATA');
          if name == 'axlen'
             mfprintf(fid, '  columns=""iteration, evaluation, sigma, ' + ...
                 'max axis length, min axis length, ' + ...
                 'all principle axes lengths (sorted square roots of eigenvalues of C)""');
          elseif name == 'fit'
            mfprintf(fid, '  columns=""iteration, evaluation, sigma, axis ratio, bestever,' + ...
                ' best, median, worst fitness function value,' + ...
                ' further objective values of best""');
          elseif name == 'stddev'
            mfprintf(fid, '  columns=""iteration, evaluation, sigma, void, void, ' + ...
                'stds==sigma*sqrt(diag(C))""');
          elseif name == 'xmean'
            mfprintf(fid, '  columns=""iteration, evaluation, void, void, void, xmean""');
          elseif name == 'xrecentbest'
            mfprintf(fid, '  columns=""iteration, evaluation, fitness, void, void, xrecentbest""');
          end
          mfprintf(fid, '>\n'); // DATA
          if name == 'xmean'
            mfprintf(fid, '%ld %ld 0 0 0 ', 0, es.counteval);
            // mfprintf(fid, '%ld %ld 0 0 %e ', es.countiter, es.counteval, fmean);
//qqq            mfprintf(fid, msprintf('%e ', genophenotransform(es.out.genopheno, es.xmean)) + '\n');
            mfprintf(fid, msprintf('%e ', es.xmean) + '\n');
          end
          mclose(fid);
        end
      end // for files
    end
  end

  es.param = p;

  es.countiter = 0; // within the restart loop
  es.stop = %F;
  es.out.stopflags = list();

endfunction
