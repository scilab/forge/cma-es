// FUNCTIONS IN THIS FILE - FUNCTIONS MOVED INTO EXTERNAL MACROS (CLAUS FUTTRUP)
//external functions, interface
//  function [es, p] = cma_new(inparam)  // constructor, p is optional the parameter struct
//  function data    = cma_plot(fignb, name_prefix, name_extension, object_variables_name, plotrange)
//  function X       = cma_ask(es, lam)  // delivers lam new points
//  function es      = cma_tell(es, X, arfitness) // update, concludes iteration
//  function flag    = cma_stop(es)     // termination criterion met?
//  function [y, x]  = cma_best(es)     // best-ever solution, see also es.out.solutions
//  function X       = %cma_ask(es)      // delivers const es.param.opt.lambda new points
//  function es      = %cma_tell(es, X, arfitness) // update, concludes iteration
//  function [y, x]  = %cma_best(es)     // best-ever solution, see also es.out.solutions

//potentially useful functions
//  function param   = check_and_merge_param(inparam, defparam) // verifies and merges two parameter lists
//  function y       = cma_genophenotransform(gp, x)   // gp=es.out.genopheno
//  function x       = cma_phenogenotransform(gp, x)   // transform into genotypic
//  function res     = cma_minnan(vec)                 // utility, compute min disregarding %nan
//internal functions
//  function p       = cma_getdefaults()
//  function gp      = cma_tlistgenopheno(typical_x, scales, x0)
//  function scales  = cma_check_initial_sizes(typical_x, x0, scales)
//  function x0      = cma_xstart(typical_x, x0, scales)
//  function           cma_annotate(xfinal, yfinal, ann) // not in use yet
//  function           cma_plotintern(name_prefix, fignb, name_extension, object_variables_name)

// see http://www.cert.fr/dcsd/idco/perso/Magni/s_sym/overload.html
// TODO:
//       o replace D completely with a vector diagD, then
//       o make separable with linear *space* complexity B,D,C
//       o (not trivial) implement cma_ask_array(es,
//         form=['col'|'row') and cma_tell_array(es, form='col') by
//         moving most code of cma_tell() into _cma_tell() leaving
//         %cma_tell(),
//         cma_tell() and cma_tell_array() merely as interface functions.
//       o constraints handling should be copied from cmaes.sci (should be easy)
//       o cma_annotate should be used to allow plotting of the genotype if need be
//       o xml docu: help_skeleton('funnam', '.'); xmltohtml(path, name);
//

// --------------------------------------------
function param = cma(dummy)
//This is a dummy for the cma library/toolbox
//that needs to be referenced once to make all functions available.
//
// See functions
//
//      cma_new
//      cma_optim
//
// help for getting started.
//
  param = cma_getdefaults();
endfunction

// CHANGES LOG
// November 2010, v0.988: replace sort with gsort, update constant cc
// May 2009, v0.997: bug fix for idxmin in plot in case of several best f-values
// June 2008, v0.993: bug fix for runcma.sce: cma_ask does not return a
//      list for lambda==1 anymore.
// June 2008, v0.991: input parameter check in cma_optim removed (is done
//      in cma_new anyway). warning on nan in cma_optim removed. option
//      readfromfile can be empty now.
// May 2008: The interface of ask and tell changed: a list of solutions rather than
//   a row array is passed now. This makes the interface safer. The possiblibity
//   to evaluate the population as a matrix is impeded.
