function [y, x] = cma_best(es)
  x = es.out.solutions.bestever.x;
  y = es.out.solutions.bestever.f;
endfunction
